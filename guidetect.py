import sys
import os
import time

from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4.phonon import Phonon

import mutagen
import mutagen.mp3
import mutagen.easyid3

import configobj
import validate

import configobj_gui
import configobj_qt

import mythread

import detect

CONFSPEC = """
silence_threshold = float(default=0.02, min=0, max=0.1) # Threshold volume for silence
min_silence_length = float(default=0.7, min=0) # Minimum length of silence between segments [s]
min_ad_length = float(default=20.0, min=0) # Minimum length of an ad [s]
max_ad_length = float(default=60.0, min=0) # Maximum length of an ad [s]
min_correlation = float(default=0.50, min=0.0, max=1.0) # Minimum correlation between segments to be considered the same
noise_volume = float(default=0.5, min=0.0, max=1.0) # Volume of generated noise
noise_length = float(default=1.0, min=0) # Length of generated noise [s]
#save_ads = boolean(default=False) # Save audio of segments considered ads. Useful for debugging.
"""

RATE = 44100
class FileChooser(QWidget):
	def __init__(self,label='',initial_text='',exists=True,parent=None):
		QWidget.__init__(self, parent)
		layout = QHBoxLayout(self)
		label = QLabel(label)
		layout.addWidget(label)
		edit = QLineEdit(initial_text)
		layout.addWidget(edit)
		browse = QPushButton('...')
		browse.clicked.connect(self.browse)
		layout.addWidget(browse)

		ok = QPushButton(app.style().standardIcon(QStyle.SP_DialogOkButton),'OK')
		ok.setEnabled(False)
		layout.addWidget(ok)
		
		edit.textChanged.connect(lambda x: ok.setEnabled(True))

		self.ok = ok
		self.edit = edit
		self.file_should_exist = exists

	def browse(self):
		if self.file_should_exist:
			path = QFileDialog.getOpenFileName(self)
		else:
			path = QFileDialog.getSaveFileName(self)

		if path != '':
			self.edit.setText(path)

class SegmentList(QWidget):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		layout = QVBoxLayout(self)
		listWidget = QListWidget(self)
		layout.addWidget(listWidget)
		
		self.listWidget = listWidget

class ItemWithPlayControl(QWidget):
	def __init__(self, text, soundfile,parent = None):
		QWidget.__init__(self, parent)
		self.playIcon = QIcon.fromTheme('media-playback-start',QIcon(':/trolltech/styles/commonstyle/images/media-play-32.png'))
		self.stopIcon = QIcon.fromTheme('media-playback-stop',QIcon(':/trolltech/styles/commonstyle/images/media-stop-32.png'))
		layout = QHBoxLayout(self)
		self.checkbox = QCheckBox(text)
		layout.addWidget(self.checkbox)
		playbtn = QPushButton(self.playIcon, 'Play')
		playbtn.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
		playbtn.clicked.connect(self.play)
		layout.addWidget(playbtn)
		self.playbtn = playbtn
		self.soundfile = soundfile
		player.currentSourceChanged.connect(self.stop)
	def sizeHint(self):
		return self.layout().sizeHint()
	def play(self):
		# Already playing
		if player.state() == Phonon.PlayingState and player.currentSource().fileName() == self.soundfile:
			player.stop()
			self.stop()
		else:
			player.setCurrentSource(Phonon.MediaSource(self.soundfile))
			player.play()
			self.playbtn.setText('Stop')
			self.playbtn.setIcon(self.stopIcon)
			self.soundfile = player.currentSource().fileName()
			
	def stop(self):
		self.playbtn.setText('Play')
		self.playbtn.setIcon(self.playIcon)
		

class MainWindow(QMainWindow):
	def __init__(self, initialPath=None, parent=None):
		QMainWindow.__init__(self, parent)
		mainWidget = QWidget(self)
		self.setCentralWidget(mainWidget)
		layout = QVBoxLayout(mainWidget)
		fileChooser = FileChooser('Input','', True)
		fileChooser.ok.clicked.connect(self.addSegments)

		# Show chooser if no initial path was given, otherwise calculate segments right away
		if initialPath == None:
			layout.addWidget(fileChooser)
		else:
			fileChooser.edit.setText(initialPath)

		segmentList = SegmentList()
		layout.addWidget(segmentList)
		buttons = QDialogButtonBox(QDialogButtonBox.Save | QDialogButtonBox.Close,Qt.Horizontal)
		buttons.button(QDialogButtonBox.Save).clicked.connect(lambda: mythread.run_task('Saving. This can take a few minutes.',self.save, parent=self))
		buttons.button(QDialogButtonBox.Save).setEnabled(False)
		buttons.button(QDialogButtonBox.Close).clicked.connect(self.close)
		layout.addWidget(buttons)
		
		self.fileChooser = fileChooser
		self.segmentList = segmentList
		self.segmentData = []
		self.buttons = buttons
		
		self.buildMenu()
		
		# Load config
		self.spec = configobj.ConfigObj(CONFSPEC.split('\n'), list_values=False)
		self.config = configobj_qt.from_QSettings(QSettings())
		self.config.configspec = self.spec
		self.config.validate(validate.Validator())

		if initialPath != None:
			self.addSegments()
		
	def buildMenu(self):
		menuBar = self.menuBar()
		menu = menuBar.addMenu('&Application')
		action = menu.addAction('&Quit')
		action.triggered.connect(QApplication.instance().closeAllWindows)
		action.setShortcut('Ctrl+Q')
		
		menu = menuBar.addMenu('&Settings')
		action = menu.addAction('&Configure...')
		action.triggered.connect(self.showConfig)
		
		menu = menuBar.addMenu('&Help')
		action = menu.addAction('&About...')
		action.triggered.connect(lambda: QMessageBox.about(self, 'About','GuiDetect 0.0'))

	def showConfig(self): 
		wnd = configobj_gui.ConfigWindow(self.config, self.spec,parent=self)
		wnd.optionChanged.connect(lambda : configobj_qt.to_QSettings(self.config))
		wnd.show()
		
	def save(self, thread):
		thread.emit(SIGNAL('valueChanged(int)'),1)
		thread.emit(SIGNAL('valueChanged(int)'),2)
		for i in range(self.segmentList.listWidget.count()):
			if thread.wantsToQuit():
				return
			(segment, is_ad, correlation) = self.segmentData[i]
			if self.segmentList.listWidget.itemWidget(self.segmentList.listWidget.item(i)).checkbox.isChecked():
				self.audio.replace_with_static(segment, self.config['noise_volume'],self.config['noise_length'])
				if not is_ad: # Not a known ad but was identified by user
					self.db.add_confirmed_signature(segment.signature)
					self.db.save()

		# Copy metadata
		info = mutagen.File(self.original_path)
		if type(info) == mutagen.mp3.MP3:
			info = mutagen.easyid3.EasyID3(self.original_path)
		
		self.audio.save() # BUG: Will not save in original sample rate, channel count etc.
		
		info2 = mutagen.File(self.original_path)
		if type(info2) == mutagen.mp3.MP3:
			info2 = mutagen.easyid3.EasyID3(self.original_path)
		for (key, value) in info.items():
			info2[key] = value
		info2.save()
	
	def addSegments(self):
		self.fileChooser.ok.setEnabled(False)
		self.segmentList.listWidget.clear()
			
		try:
			self.segmentData = mythread.run_task('Analyzing. This can take a few minutes.', self.dostuff, parent=self)
		except Exception, e:
			QMessageBox.critical(self,'An error occured',str(e))
			return
			
		for (segment, is_ad, correlation) in self.segmentData:
			start = segment.start
			end = segment.end
			soundfile = segment.soundfile
			item = QListWidgetItem()

			self.segmentList.listWidget.addItem(item)
			text = '%.0fs..%.0fs'%(float(start)/float(self.audio.rate),float(end)/float(self.audio.rate))
			if correlation > 0.0:
				text += ' (%.2f)'%correlation
			widget = ItemWithPlayControl(text, segment.soundfile)

			widget.checkbox.setChecked(is_ad)

			item.setSizeHint(widget.sizeHint())
			self.segmentList.listWidget.setItemWidget(item,widget)
		self.buttons.button(QDialogButtonBox.Save).setEnabled(True)
		
	def dostuff(self, thread):
		segments = []
		thread.emit(SIGNAL('valueChanged(int)'),1)
		thread.emit(SIGNAL('valueChanged(int)'),2)
		filepath = str(self.fileChooser.edit.text())
		self.original_path = filepath
		
		info = mutagen.File(filepath)
		if type(info) == mutagen.mp3.MP3:
			info = mutagen.easyid3.EasyID3(filepath)
		try:
			album = info['album'][0]
		except (KeyError, TypeError):
			QMessageBox.warning(self, 'Warning!','Could not determine album name.')
			album = 'uknnown'
			
		print album

		db = detect.SignatureDB(album+'.db')
		self.db = db
		
		audio = detect.AudioFile(filepath, RATE, 1)
		self.audio = audio
		try: # For debugging purposes it's sometimes sensible to not actually have a thread
			if thread.wantsToQuit():
				return []
		except AttributeError:
			pass
		c = self.config
		for segment in audio.find_segments(True, c['silence_threshold'],c['min_silence_length'],c['min_ad_length'],c['max_ad_length']):
			try:
				if thread.wantsToQuit():
					return []
			except AttributeError:
				pass
			(is_ad, correlation) = db.check_signature(segment.signature)
			segments.append((segment, is_ad, correlation))

		db.file_checked()
		db.save()
		return segments
	def close(self):
		# Remove temporary files
		for (segment, _, _) in self.segmentData:
			segment.remove_soundfile()
		return QMainWindow.close(self)
			
app = QApplication(sys.argv)
app.setApplicationName('guidetect')
app.setOrganizationName('pafcu')
player = Phonon.createPlayer(Phonon.NoCategory)
try:
	initial_path = sys.argv[1]
except IndexError:
	initial_path = None
wnd = MainWindow(initial_path)
wnd.show()
app.exec_()
