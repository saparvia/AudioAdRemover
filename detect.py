import sys
import copy
import cPickle
import os
import shutil
import tempfile
import subprocess
import logging
import time

log = logging.getLogger('main')
log.setLevel(logging.DEBUG)

import scikits.audiolab as audiolab
import audioprocessing
import numpy as np

def _correlate(bands1,bands2):
	c = 0.0
	for r1, r2 in zip(bands1,bands2):
		c += np.corrcoef(r1,r2)[1][0]
	return c / len(bands1)

def _silent_parts(samples, threshold, min_length):
	start = -1
	silent_segments = []
	threshold = threshold*(2**16/2)
	for idx,x in enumerate(samples):
		if start < 0 and abs(x) < threshold:
			start = idx
		elif start >= 0 and abs(x) >= threshold:
			dur = idx-start
			if dur >= min_length:
				yield (start, idx)
			start = -1

def _fast_silent_parts(samples, threshold, min_length):
	threshold = threshold*(2**16/2)
	condition = np.abs(samples) < threshold
	d = np.diff(condition)
	idx, = d.nonzero() 
	idx += 1

	if condition[0]:
		idx = np.r_[0, idx]

	if condition[-1]:
		idx = np.r_[idx, condition.size - 1]

	idx.shape = (-1,2)
	parts =[(start,end) for (start, end) in idx if end - start > min_length] 
	return parts

def wavread(infile, rate=None, channels=None):
	log.debug('Reading file %s',infile)
	# Need to do some conversion
	temp = None
	if rate != None or channels != None or infile.endswith('.mp3'):
		args = ['ffmpeg','-i',infile]
		if rate != None:
			args.append('-ar')
			args.append(str(rate))
		if channels != None:
			args.append('-ac')
			args.append(str(channels))
		temp = tempfile.mktemp('.wav','a')
		args.append(temp)
		log.debug('Calling ffmpeg: %s',args)
		subprocess.call(args,stderr=subprocess.PIPE)
		infile = temp

	try:
		a = audiolab.Sndfile(infile, channels = 1, samplerate = 44100)
		frames = a.read_frames(a.nframes,dtype = np.int16)
		rate = a.samplerate
		enc = a.encoding
	except IOError, err:
		if 'unknown format' in str(err):
			log.error('Unsupported audio format')
			sys.exit(1)
		else:
			raise err
	if temp != None:
		os.unlink(temp)

	return (np.ma.asarray(frames), rate, enc)

def wavwrite(frames, outfile, rate, enc):
	log.debug('Writing file %s',outfile)
	temp = None
	if outfile.endswith('.mp3'):
		temp = tempfile.mktemp('.wav','b')
		audiolab.wavwrite(frames.compressed(), temp, rate, enc)
		args = ['ffmpeg','-y','-i',temp,outfile]
		log.debug('Calling ffmpeg: %s',args)
		subprocess.call(args,stderr=subprocess.PIPE)
		os.unlink(temp)
	else:
		audiolab.wavwrite(frames.compressed(), outfile, rate, enc)

def _signature_matches_any(signature, others, min_correlation=0.5):
	correlation = 0
	maxcorr = 0
	for sig in others:
		bandslen = min(sig.bands.shape[1],signature.bands.shape[1])
		bands1 = sig.bands[:,:bandslen-1]
		bands2 = signature.bands[:,:bandslen-1]
		correlation = _correlate(bands1, bands2)
		log.debug('Correlation: %s',correlation)
		maxcorr = max(maxcorr, correlation)
		if correlation > min_correlation: # The current signature is similar to a potential ad, so it probably really is an ad
			log.info('Found ad with correlation %f',correlation)
			return (sig, correlation)

	return (None, 0)

class SignatureDB(object):
	def __init__(self, dbfile):
		# Try to load db of past signatures
		try:
			db = cPickle.load(open(dbfile))
			log.debug("Loaded db from %s, %d files processed so far", dbfile, db['files_checked'])
		except Exception:
			log.debug("Using new db")
			# Create clean db
			db = {}
			db['potential'] = {}
			db['confirmed'] = {}
			db['files_checked'] = 0
		self.dbfile = dbfile
		self.db = db

	def save(self):
		log.debug('Saving db')
		cPickle.dump(self.db, open(self.dbfile, 'w'), -1)

	def clean(self, expire=2):
		log.debug('Cleaning db')
		self.db['potential'] = dict([(key, value) for key, value in self.db['potential'].items() if self.db['files_checked'] - value[1] < expire])

	def check_signature(self, signature):
		# Compare signature against past signatures
		db = self.db
		log.debug('Checking signature against confirmed ads')
		matching_sig, correlation = _signature_matches_any(signature, db['confirmed'])
		if matching_sig != None:
			if correlation == 1.0:
				return (True, correlation)
			else:
				db['confirmed'][matching_sig][0] += 1
				return (True, correlation)

		log.debug('Checking signature against potential ads')
		matching_sig, correlation = _signature_matches_any(signature, db['potential'])
		if matching_sig != None:
			if correlation == 1.0:
				return (False, correlation)
			else:
				self.add_confirmed_signature(signature)
				return (True, correlation)

		# Didn't match any known
		db['potential'][signature] = [1, db['files_checked']]

		return (False,0)

	def add_confirmed_signature(self, signature):
		self.db['confirmed'][signature] = [1, self.db['files_checked']] # Add to confirmed

	def file_checked(self):
		self.db['files_checked'] += 1

class Segment:
	def __init__(self, start, end, frames, save_soundfile=False, secs_per_block=2):
		self.start = start
		self.end = end
		self.frames = frames

		tmpwav = tempfile.mktemp('.wav')
		self.soundfile = tmpwav
		audiolab.wavwrite(self.frames, tmpwav, 44100, 'pcm16')
		log.debug('Calculating butterscotch')
		self.signature = audioprocessing.butterscotch(tmpwav, secs_per_block=secs_per_block)
		log.debug('Done')
		if not save_soundfile:
			os.unlink(tmpwav)
	def remove_soundfile(self):
		os.unlink(self.soundfile)


class AudioFile(object):
	def __init__(self, filepath, rate=None, channels=None):
		self.frames, self.rate, self.enc = wavread(filepath, rate, channels)		
		self.infile = filepath

	def find_segments(self, save_soundfile=False, silence_threshold=0.02, min_silence=0.9, min_ad_length=20, max_ad_length=90):
		log.debug('Segmenting...')
		frames = self.frames
		rate = self.rate
		if frames.size < 2e9/2: # Limits memory usage
			silence_f = _fast_silent_parts # Change to silent_parts if low on memory
		else:
			silence_f = _silent_parts
			log.info('Using slow analysis')

		segments = []
		prev_end = 0
		for (silence_start, silence_end) in silence_f(frames, silence_threshold, min_silence * rate):
			silence_length = silence_end - silence_start
			length = silence_start - prev_end
			if  silence_length > min_silence * self.rate:
				if length > min_ad_length * rate and length < max_ad_length * rate and silence_start != 0:
					log.info("Found potential ad segment between %f and %f (%d %d)", float(prev_end)/rate, float(silence_start)/rate, prev_end, silence_start)
					segment_start = prev_end
					segment_end = silence_start+0.1*rate # Add 0.1s to compensate for not complete silence right away
					yield Segment(segment_start, segment_end, self.frames[segment_start:segment_end], save_soundfile)
			prev_end = silence_end


	def replace_with_static(self, segment, noise_volume=0.9, noise_len=1.5):
		start = segment.start
		end = segment.end 
		log.debug('Replacing segment from %s to %s with noise'%(start, end))
		noise = noise_volume * (2**16/2)* np.random.random_sample(noise_len*self.rate)
		self.frames[start:start+noise.size] = noise
		self.frames[start+noise.size:end] = np.ma.masked # Mask out whatever shouldn't be heard


	def save(self, outfile=None):
		if outfile == None:
			outfile = self.infile
		wavwrite(self.frames, outfile, self.rate, self.enc)

def clean(infile, dbfile, outfile):
	audio = AudioFile(infile, 44100, 1)
	db = SignatureDB(dbfile)

	for segment in audio.find_segments():
		(is_ad, correlation) = db.check_signature(segment.signature)
		if is_ad:
			audio.replace_with_static(segment)

	db.clean()
	db.file_checked()
	audio.save(outfile)
	db.save()
	return True

if __name__ == '__main__':
	log.addHandler(logging.StreamHandler(sys.stderr))
	clean(sys.argv[1], sys.argv[2], sys.argv[1]+'_cleaned.wav')
